define ->
  class WidgetBanner extends require 'inject/widget'
    $columns: []
    
    constructor: (widgetModel) ->
      super(widgetModel)
      @$columns = []
    
    update: (widgetBlock) ->
      $container = widgetBlock.$element.children('.row')
      attributes = widgetBlock.attributes
      $columns = $container.children('div')
      
      # Reset container style
      $container.attr('style', '')
      $container.attr('class', 'row')
      
      # Save current state
      for column in $columns
        @$columns.push $(column).clone()
              
      # Clear contents
      $container.html('')
      
      # Recreate columns
      for i in [0..attributes.columns-1]
          
        # Create column
        if @$columns[i]
          # Load existing column contents
          $column = @$columns[i].clone().removeAttr('class')
          # add drop container if not exists
          if not $column.find('._typeloft_widget_drop_container').length
            $('<div>').addClass('_typeloft_widget_drop_container').prependTo($column)
        else
          # Create new column with droppable container
          $column = $('<div>')
          $('<div>').addClass('_typeloft_widget_drop_container').appendTo($column)

        # Set column class
        $column.addClass("column col-sm-#{(12 / parseInt(attributes.columns))}")

        # Append to row
        $column.appendTo($container)
