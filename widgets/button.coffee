define ->
  class WidgetButton extends require 'inject/widget'
    
    update: (widgetBlock) ->
      $container = widgetBlock.$element.children('a.btn')
      attributes = widgetBlock.attributes
      
      # Reset style
      $container.attr('class', 'btn _typeloft_editable')
      
      # Set callout style
      $container.addClass("btn-" + attributes.style)
      $container.addClass(attributes.type)
