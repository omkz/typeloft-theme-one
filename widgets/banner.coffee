define ->
  class WidgetBanner extends require 'inject/widget'
    
    update: (widgetBlock) ->
      $container = widgetBlock.$element.children('.banner')
      attributes = widgetBlock.attributes
      
      $container.attr('class', 'banner')
      $container.addClass("banner-#{attributes.style}")
