define ->
  class WidgetImage extends require 'inject/widget'
        
    update: (widgetBlock) ->
      attributes = widgetBlock.attributes
      $container = widgetBlock.$element.children('.image-widget')
      $image = $container.children('.image')
      $image = $container.children('a').children('.image') if $image.length == 0
      
      # Reset container style
      $image.attr('style', '')
      $image.attr('class', 'image')
      $image.attr('src', attributes.source)
      $image.addClass(attributes.style)
  
    placeDropContainer: (widgetBlock, $dropContainer) ->
      $container = widgetBlock.$element.children('.image-widget')
      $dropTarget = $container.children('.image-drop-target')
      
      # Return false if widget already exists
      # console.log('children', $dropTarget.children('._typeloft_widget').length);

      if $dropTarget.children('._typeloft_widget').length > 0
        # console.log('ALREADY EXISTS!')
        return false
      
      # Append widget and return true
      $dropContainer.appendTo($dropTarget)
      true
