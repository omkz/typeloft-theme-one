define ->
  class WidgetContainer extends require 'inject/widget'
    
    update: (widgetBlock) ->
      $container = widgetBlock.$element.children('div')
      attributes = widgetBlock.attributes
      
      # Reset container style
      $container.attr('class', 'paragraph _typeloft_editable _typeloft_widget_autoselect')
      
      # Set paragraph style
      $container.addClass(attributes.style)
      $container.addClass("align-#{attributes.align}")
