define ->
  class WidgetContainer extends require 'inject/widget'
        
    update: (widgetBlock) ->
      $container = widgetBlock.$element.children('div')
      $href = $container.children('a')
      attributes = widgetBlock.attributes
      
      # Reset mailto style
      $container.attr('class', '_typeloft_editable _typeloft_widget_autoselect')
      
      # Set mailto style
      $href.attr("href", "mailto:#{attributes.target_email}")
      $href.text(attributes.link_text)
      $container.addClass(attributes.style)
      $container.addClass("align-#{attributes.align}")

