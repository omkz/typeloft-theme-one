define ->
  class WidgetContainer extends require 'inject/widget'
        
    update: (widgetBlock) ->
      $container = widgetBlock.$element.children('header')
      $heading = $container.children('h1, h2, h3, h4, h5')
      attributes = widgetBlock.attributes
      
      # Re-parse element tag
      $heading.replaceWith($("<#{attributes.type}>").html($heading.html()))
      $heading = $container.children('h1, h2, h3, h4, h5')
      
      # Reset container style
      $container.removeAttr('class')
      $heading.attr('class', '_typeloft_editable _typeloft_widget_autoselect')
      
      # Set heading style
      $container.addClass(attributes.style)
      $container.addClass("align-#{attributes.align}")
      