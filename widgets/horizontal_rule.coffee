define ->
  class WidgetHorizontalRule extends require 'inject/widget'
        
    update: (widgetBlock) ->
      attributes = widgetBlock.attributes
      $container = widgetBlock.$element.children('hr')
      
      # Reset hr style
      $container.attr('class', '_typeloft_editable _typeloft_widget_autoselect')
      
      # Set hr style
      $container.addClass(attributes.style)
