define ->
  class WidgetContainer extends require 'inject/widget'
    imageData: []
        
    update: (widgetBlock) ->
      $container = widgetBlock.$element.children('.one-container')
      $image = $container.children('.one-container-image')
      attributes = widgetBlock.attributes
      
      # Reset container style
      $container.attr('style', '')
      $container.attr('class', 'one-container')
      $image.attr('style', '')
      $image.attr('class', 'one-container-image')

      # Set background color
      if attributes.background_color and attributes.background_color != 'none'
        $container.css('background-color', "##{attributes.background_color}")

      # Set background image
      if attributes.image_source and attributes.image_source != ''
        $container.css('background-image', "url(#{attributes.image_source})")
      else if attributes.pattern != 'none'
        $container.addClass("pattern-#{attributes.pattern}")
      
      # Set minimum height
      $image.css('min-height', attributes.min_height)  if attributes.min_height and attributes.min_height != ''
      
      $image.addClass("align-"+attributes.align)
      
      # Set padding
      $image.css
        paddingLeft: attributes.padding_left
        paddingRight: attributes.padding_right
        paddingTop: attributes.padding_top
        paddingBottom: attributes.padding_bottom

      # Set images
      @resetImageData()
      @addImage("themes/one/images/#{attributes.stripes_style}-tl.svg", "scroll", "top left", "no-repeat", "25em") if attributes.stripes_top_left
      @addImage("themes/one/images/#{attributes.stripes_style}-tr.svg", "scroll", "top right", "no-repeat", "25em") if attributes.stripes_top_right
      @addImage("themes/one/images/#{attributes.stripes_style}-bl.svg", "scroll", "bottom left", "no-repeat", "25em") if attributes.stripes_bottom_left
      @addImage("themes/one/images/#{attributes.stripes_style}-br.svg", "scroll", "bottom right", "no-repeat", "25em") if attributes.stripes_bottom_right
      @addImage("themes/one/images/pattern/#{attributes.pattern}.png", "scroll", "top left", "repeat", "auto") if attributes.pattern and attributes.pattern != 'none'
      @applyImages($image)
    
    resetImageData: ->
      @imageData = []
    
    addImage: (url, attachment='scroll', position='center center', repeat='no-repeat', size='cover') ->
      @imageData.push "url(#{url}) #{position} / #{size} #{repeat}"

    applyImages: ($element) ->
      $element.css('background', @imageData.join(', '))
